# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from subscriptions.views import SubscriptionList
from subscriptions.views import SubscriptionDetail
from subscriptions.views import SubscriptionCreate
from subscriptions.views import SubscriptionUpdate
from subscriptions.views import SubscriptionDelete
from django.contrib import admin

urlpatterns = patterns(
    'subscriptions.views',
    url(r'^$', 'home', name='home'),

    url(r'^subscription/$', SubscriptionList.as_view(), name='subscription_list'),

    url(r'^subscription/add/$', SubscriptionCreate.as_view(), name='subscription_add'),

    url(r'^subscription/(?P<pk>\d+)/$',
        SubscriptionDetail.as_view(), name='subscription_detail'),

    url(r'^subscription/edit/(?P<pk>\d+)/$',
        SubscriptionUpdate.as_view(), name='subscription_edit'),

    url(r'^subscription/delete/(?P<pk>\d+)/$',
        SubscriptionDelete.as_view(), name='subscription_delete'),

    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
