# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Subscription, Domain


class DomainInline(admin.TabularInline):
    model = Domain
    extra = 1


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'cpf', 'email', 'phone', 'created')
    date_hierarchy = 'created'
    search_fields = ('name', 'cpf', 'email')
    list_filter = ('uf',)
    inlines = [DomainInline, ]


class DomainAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'subscription')

admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Domain, DomainAdmin)
