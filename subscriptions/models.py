# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
# List of values for use in choices
from .lists import uf_list


class Subscription(models.Model):
    name = models.CharField(_('nome'), max_length=100)
    address = models.CharField(_(u'endereço'), max_length=80)
    district = models.CharField(_('bairro'),
                                max_length=80,
                                null=True,
                                blank=True)
    city = models.CharField(_('cidade'), max_length=80)
    uf = models.CharField(_('UF'), max_length=2, choices=uf_list)
    cep = models.CharField(_('CEP'), max_length=9, null=True, blank=True)
    cpf = models.CharField(_('CPF'), max_length=11, unique=True)
    email = models.EmailField(_('email'), blank=True)
    phone = models.CharField(_('telefone'), max_length=20, blank=True)
    created = models.DateTimeField(_('criado em'),
                                   auto_now_add=True,
                                   auto_now=False)
    modified = models.DateTimeField(_('modificado em'),
                                    auto_now_add=False,
                                    auto_now=True)

    class Meta:
        ordering = ['name']
        verbose_name = _(u'inscrição')
        verbose_name_plural = _(u'inscrições')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        ''' retorna o id '''
        return reverse_lazy('subscription_detail', kwargs={'pk': self.pk})

    def count_domain(self):
        ''' conta os domínios por pessoa '''
        return self.domain_subscription.count()


class Domain(models.Model):
    domain = models.CharField(_(u'domínio'),
                              max_length=100,
                              null=True,
                              blank=True)
    subscription = models.ForeignKey('Subscription',
                                     verbose_name=u'inscrição',
                                     related_name='domain_subscription')

    class Meta:
        ordering = ['domain']
        verbose_name = _(u'domínio')
        verbose_name_plural = _(u'domínios')

    def __str__(self):
        return self.domain
