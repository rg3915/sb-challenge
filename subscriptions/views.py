# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView

from .models import Subscription, Domain
from .forms import SubscriptionForm, DomainFormSet
from .mixins import LoginRequiredMixin, CounterMixin


def home(request):
    return render(request, 'index.html')


class FormsetMixin(object):
    object = None

    def get(self, request, *args, **kwargs):
        if getattr(self, 'is_update_view', False):
            self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class)
        return self.render_to_response(self.get_context_data(form=form, formset=formset))

    def post(self, request, *args, **kwargs):
        if getattr(self, 'is_update_view', False):
            self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def get_formset_class(self):
        return self.formset_class

    def get_formset(self, formset_class):
        return formset_class(**self.get_formset_kwargs())

    def get_formset_kwargs(self):
        kwargs = {
            'instance': self.object
        }
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def form_valid(self, form, formset):
        self.object = form.save()
        formset.instance = self.object
        formset.save()
        return redirect(self.object.get_absolute_url())

    def form_invalid(self, form, formset):
        return self.render_to_response(self.get_context_data(form=form, formset=formset))


class SubscriptionCreate(FormsetMixin, CreateView):
    template_name = 'subscriptions/subscription_form.html'
    form_class = SubscriptionForm
    formset_class = DomainFormSet


class SubscriptionList(LoginRequiredMixin, CounterMixin, ListView):
    ''' No Django 1.8 não precisa mais, desde que o template termine com <model>_list.html
    template_name = 'subscriptions/subscription_list.html' '''
    model = Subscription
    context_object_name = 'subscriptions'
    paginate_by = 10

    def get_queryset(self):
        s = Subscription.objects.all()
        q = self.request.GET.get('search_box')
        ''' buscar por nome '''
        if q is not None:
            s = s.filter(name__icontains=q)
        return s


class SubscriptionDetail(DetailView):
    ''' No Django 1.8 não precisa mais, desde que o template termine com <model>_detail.html
    template_name = 'subscriptions/subscription_detail.html' '''
    model = Subscription

    def get_context_data(self, **kwargs):
        context = super(SubscriptionDetail, self).get_context_data(**kwargs)
        ''' retorna os domínios do inscrito '''
        domains = Domain.objects.filter(subscription=self.kwargs['pk'])
        context['domains'] = domains
        return context


class SubscriptionUpdate(FormsetMixin, UpdateView):
    template_name = 'subscriptions/subscription_form.html'
    is_update_view = True
    model = Subscription
    form_class = SubscriptionForm
    formset_class = DomainFormSet


class SubscriptionDelete(DeleteView):
    model = Subscription
    success_url = reverse_lazy('subscription_list')
