from django.test import TestCase
from django.db import IntegrityError
from datetime import datetime
from subscriptions.models import Subscription


class SubscriptionTest(TestCase):

    def setUp(self):
        self.obj = Subscription(
            name='Regis da Silva Santos',
            address='Rua Canário, 187',
            district='Ayrosa',
            city='Osasco',
            uf='SP',
            cep='06293-090',
            cpf='00000000000',
            email='regis.santos.100@gmail.com',
            phone='11 98765-4321',
        )

    def test_create(self):
        '''
        Subscription must have name, address, district, city, uf, cep,
        cpf, email, phone, created, modified
        '''
        self.obj.save()
        self.assertEqual(1, self.obj.pk)

    def test_has_created_at(self):
        'Subscription must have automatic created.'
        self.obj.save()
        self.assertIsInstance(self.obj.created, datetime)


class SubscriptionUniqueTest(TestCase):

    def setUp(self):
        Subscription.objects.create(
            name='Regis da Silva Santos',
            address='Rua Canário, 187',
            district='Ayrosa',
            city='Osasco',
            uf='SP',
            cep='06293-090',
            cpf='00000000000',
            email='regis.santos.100@gmail.com',
            phone='11 98765-4321',
        )

    def test_cpf_unique(self):
        'CPF must be unique'
        s = Subscription(
            name='Regis da Silva Santos',
            address='Rua Canário, 187',
            district='Ayrosa',
            city='Osasco',
            uf='SP',
            cep='06293-090',
            cpf='00000000000',
            email='segundoemail@gmail.com',
            phone='11 98765-4321',
        )
        self.assertRaises(IntegrityError, s.save)

    def test_email_can_repeat(self):
        'Email is not unique anymore.'
        s = Subscription.objects.create(
            name='Regis da Silva Santos',
            address='Rua Canário, 187',
            district='Ayrosa',
            city='Osasco',
            uf='SP',
            cep='06293-090',
            cpf='00000000011',
            email='segundoemail@gmail.com',
            phone='11 98765-4321',
        )
        self.assertEqual(2, s.pk)
