from django.test import TestCase
from subscriptions.models import Subscription


class HomeTest(TestCase):

    def setUp(self):
        self.resp = self.client.get('/')

    def test_get(self):
        ''' get / deve retornar status code 200. '''
        self.assertEqual(200, self.resp.status_code)

    def test_template(self):
        ''' Home deve usar template index.html '''
        self.assertTemplateUsed(self.resp, 'index.html')


class SubscriptionFormTest(TestCase):

    def setUp(self):
        self.resp = self.client.get('/subscription/add/')

    def test_get(self):
        ''' get /subscription/add/ deve retornar status code 200. '''
        self.assertEqual(200, self.resp.status_code)

    def test_template(self):
        ''' SubscriptionCreate deve usar template subscription_form.html '''
        self.assertTemplateUsed(
            self.resp, 'subscriptions/subscription_form.html')
