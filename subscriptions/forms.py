from django import forms
from django.utils.translation import ugettext as _
from django.core.exceptions import ValidationError
from django.forms.models import inlineformset_factory
from .models import Subscription, Domain

from input_mask.widgets import InputMask
from input_mask.contrib.localflavor.br.widgets import BRCPFInput, BRZipCodeInput


class DDDFone(InputMask):
    mask = {'mask': '(99) 99999-9999'}


def CPFValidator(value):
    # faz algumas validações de CPF
    if not value.isdigit():
        raise ValidationError(_(u'CPF deve conter apenas números'))
    if len(value) != 11:
        raise ValidationError(_(u'CPF deve ter 11 números'))


class SubscriptionForm(forms.ModelForm):
    cep = forms.CharField(widget=BRZipCodeInput)

    class Meta:
        model = Subscription
        fields = '__all__'
        widgets = {
            # 'cpf': BRCPFInput,
            'phone': DDDFone,
        }

    def __init__(self, *args, **kwargs):
        super(SubscriptionForm, self).__init__(*args, **kwargs)

        self.fields['cpf'].validators.append(CPFValidator)

    def clean_name(self):
        # retorna o nome com a primeira letra maiúscula.
        name = self.cleaned_data['name']
        words = map(lambda w: w.capitalize(), name.split())
        capitalized_name = ' '.join(words)
        return capitalized_name

DomainFormSet = inlineformset_factory(
    Subscription, Domain, extra=2, min_num=1, fields=('domain',))
