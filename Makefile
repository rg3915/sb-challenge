install:
	pip install -r requirements.txt

migrate:
	./manage.py makemigrations
	./manage.py migrate

createuser:
	./manage.py createsuperuser --username='admin' --email=''

selenium_subscription:
	python selenium/selenium_subscription.py

backup:
	./manage.py dumpdata subscriptions --format=json --indent=2 > fixtures.json

load:
	./manage.py loaddata fixtures.json

initial: install migrate createuser load
