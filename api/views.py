from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from subscriptions.models import Subscription
from api.serializers import SubscriptionSerializer


@api_view(['GET', 'POST'])
def subscription_list_rest(request):
    """
    List all subscription, or create a new subscription.
    """
    if request.method == 'GET':
        s = Subscription.objects.all()
        serializer = SubscriptionSerializer(s, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = SubscriptionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def subscription_detail_rest(request, pk):
    """
    Get, udpate, or delete a specific subscription
    """
    from pdb import set_trace
    # set_trace()
    try:
        s = Subscription.objects.get(pk=pk)
    except Subscription.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SubscriptionSerializer(s)
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = SubscriptionSerializer(s, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serilizer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        s.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
