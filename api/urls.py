# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns(
    'api.views',
    url(r'^subscription/$', 'subscription_list_rest',
        name='subscription_list_rest'),

    url(r'^subscription/(?P<pk>\d+)/$',
        'subscription_detail_rest', name='subscription_detail_rest'),
)
