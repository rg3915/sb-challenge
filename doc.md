# Desafio Site Blindado

## Nomes considerados

* Ambiente: sb-challenge
* Projeto: sbproject
* App: subscriptions
* Subscription: name, address, district, city, uf, cep, cpf, email, phone, created, modified
* Domain: domain, subscription (fk)

## Como rodar o projeto

	$ git clone https://rg3915@bitbucket.org/rg3915/sb-challenge.git
	$ virtualenv -p python3 sb-challenge
	$ cd sb-challenge
	$ source bin/activate
	$ make initial
	$ ./manage.py runserver

## Testes

	$ ./manage.py test

## Selenium

	$ make selenium_subscription

## Lista de inscritos

A lista de inscritos requer autenticação.

**user**: admin

**password**: admin

### Excluir uma inscrição

Para excluir uma inscrição entre nos **detalhes** de cada um e clique no botão excluir (**requer autenticação**).

## api rest

curl http://localhost:8000/api/subscription/

curl -X POST http://localhost:8000/api/subscription/ -d "name=Regis da Silva Santos&cep=06293-090&address=Rua Canario, 187&city=Osasco&uf=SP&cpf=37792262006&email=regis.santos.100@gmail.com"

curl -X PUT http://localhost:8000/api/subscription/1/ -d "name=Regis&cep=06293-090&address=Rua Canario, 187&city=Osasco&uf=RJ&cpf=37792262050&email=regis.santos.100@gmail.com"

curl -X DELETE http://localhost:8000/api/subscription/1/