import random
import csv

address_list = []

''' Lendo os dados de enderecos_.csv '''
with open('selenium/enderecos_.csv', 'r') as f:
    r = csv.DictReader(f)
    for dct in r:
        address_list.append(dct)
    f.close()


def gen_address():
    ''' retorna um endereço '''
    i = random.randint(1, len(address_list))
    return address_list[i]
