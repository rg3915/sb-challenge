# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from gen_names import gen_full_name
from gen_address import gen_address
from gen_random_values import *

customer = webdriver.Firefox()
customer.get('http://localhost:8000/subscription/add/')

name = gen_full_name()
search = customer.find_element_by_id('id_name')
search.send_keys(name)
time.sleep(0.3)

address_item = gen_address()

cep = address_item['cep']
search = customer.find_element_by_id('id_cep')
search.send_keys(cep)
time.sleep(0.3)

address = address_item['address']
search = customer.find_element_by_id('id_address')
search.send_keys(address)
time.sleep(0.3)

district = address_item['district']
search = customer.find_element_by_id('id_district')
search.send_keys(district)
time.sleep(0.3)

city = address_item['city']
search = customer.find_element_by_id('id_city')
search.send_keys(city)
time.sleep(0.3)

uf = address_item['uf']
if uf == 'SP':
    uf = 'São Paulo'
search = customer.find_element_by_id('id_uf')
search.send_keys(uf)
time.sleep(0.3)

cpf = gen_doc()
search = customer.find_element_by_id('id_cpf')
search.send_keys(cpf)
time.sleep(0.3)

email = ''.join(name.lower().split()) + '@example.com'
search = customer.find_element_by_id('id_email')
search.send_keys(email)
time.sleep(0.3)

phone = gen_phone()
search = customer.find_element_by_id('id_phone')
search.send_keys(phone)
time.sleep(0.3)

domain = gen_site()
search = customer.find_element_by_id('id_domain_subscription-0-domain')
search.send_keys(domain)
time.sleep(0.3)

domain = gen_site()
search = customer.find_element_by_id('id_domain_subscription-1-domain')
search.send_keys(domain)
time.sleep(0.3)

domain = gen_site()
search = customer.find_element_by_id('id_domain_subscription-2-domain')
search.send_keys(domain)
time.sleep(0.3)

button = customer.find_element_by_id('id_submit')
button.click()
