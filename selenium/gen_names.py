import random
import names
import rstr

''' Gera nomes randômicos '''


def gen_first_name():
    return names.get_first_name()


def gen_last_name():
    return names.get_last_name()


def gen_full_name():
    return '%s %s' % (gen_first_name(), gen_last_name())
